#configuration for this game
#gamename. only one game with this gamename can run on the machine at the same
#time.
GAMENAME="FL2"
#two folders are expected in $ROOTPATH, $GAMENAME holding configuration files
#and $GAMENAME-stuff, for other stuff.
ROOTPATH="/home/malte/freeciv/games/"
FREECIVBINARY="/home/malte/freeciv/freeciv-2_5_1_LT/bin/freeciv-server"
PORT=3690
STATSDIR="/home/malte/public_html/${GAMENAME}/stats/"
