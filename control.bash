#!/bin/bash


#using set later will overwrite $1, $2 etc. so we start by saving those.
OPTION="$1"
CMDSAVE="$2"
CMD="$3"

#find out where we are...
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD";  fi
echo "dir this script is located in: $DIR"

#load configuration
source "$DIR/configuration.sh"


#print out configuration
echo "Configuration:"
echo "GAMENAME: ${GAMENAME}"
echo "ROOTPATH: ${ROOTPATH}"
echo "FREECIVBINARY: ${FREECIVBINARY}"
echo "PORT: ${PORT}"


#some folders
GAMEHOMEDIR="${ROOTPATH}/${GAMENAME}"
SERVERNAME="Freeciv-$GAMENAME"
SCREENNAME="screen${SERVERNAME}"
SAVEFOLDER="${ROOTPATH}/${GAMENAME}-stuff/saves/"
SNAPSHOTFOLDER="${ROOTPATH}/${GAMENAME}-stuff/snapshots/"


#this script better not run in parallel, so do locking.
set -e
LOCKFILE="${ROOTPATH}/${GAMENAME}-stuff/tmp/control.lock"
exec 200>$LOCKFILE
flock -n 200 || exit
pid=$$
echo $pid 1>&200
set +e
#after this point we can be certain no other control.bash is running.

echo "SAVEFOLDER: ${SAVEFOLDER}"

#we need to find out how many savegames we have (from those made by the
#cronjob) and which one is the last.
set -- ${SAVEFOLDER}/*
eval "LAST_SAVE_NAME=\${$#}"
eval "FIRST_SAVE_NAME=\${1}"
eval "SECOND_SAVE_NAME=\${2}"
eval "NUMBER_SAVES=$#"
#output the number of savefiles. if you can see this, then execution
#wasn't aborted due to locking.
echo "There are $NUMBER_SAVES savefiles stored."
echo "Last savefile is ${LAST_SAVE_NAME}"

#options for the freeciv server
#add option --Newusers below for start of game (to make people able to register)
COMMAND="${FREECIVBINARY} --exit-on-end --port ${PORT} --Announce none --read ${GAMENAME}.serv --Database ${ROOTPATH}/${GAMENAME}/fc_auth.conf --auth -f ${LAST_SAVE_NAME} -s $SNAPSHOTFOLDER" 
FREECIV_PATH="/home/malte/freeciv/games/fl1/data/"
COMMANDFIRSTTIME="${FREECIVBINARY} --exit-on-end --port ${PORT} --Announce none --read ${GAMENAME}.serv --Database ${ROOTPATH}/${GAMENAME}/fc_auth.conf --auth -s $SNAPSHOTFOLDER" 


#some commands for the server
SAVECOMMAND="save ${SAVEFOLDER}$(date +%Y%m%d%H%M)^M"
QUITCOMMAND="quit^M"
INITAUTHCOMMAND="fcdb lua sqlite_createdb()^M"

#check if the freeciv server is already running
RUNNING=`screen -list $SCREENNAME | grep -c "No Sockets"`

#where we will write stuff we get from inside the screen session.
SCREENOUTPUTFILE="$CMDSAVE"

case "$OPTION" in
	help)
		echo "This script controls the Freeciv Server"
		echo "Usage:"
		echo "./control.bash init      -- set up auth database and quit server again"
		echo "./control.bash startnew  -- start the server without loading savefile"
		echo "./control.bash start     -- start the server"
		echo "./control.bash stop      -- stop the server after saving"
		echo "./control.bash halt      -- stop the server without saving"
		echo "./control.bash maintain  -- save if the server is running, else restart"
		echo "./control.bash control   -- attach to the server process"
		echo "./control.bash save      -- save if the server is running"
		echo "./control.bash clean     -- clean the save directory (only 50 savegames will remain)"
		echo "./control.bash command FILE CMD -- send CMD to the freeciv server, and write output to file FILE"
	;;
	init)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server was not running. Starting..."
			echo "Starting $SERVERNAME..."
                        screen -d -m -S "$SCREENNAME" $COMMANDFIRSTTIME	
			sleep 10
		fi
		echo "Server is runnning. Sending command to initialize auth database."
		#echo -e "\"$SAVECOMMAND\""
		screen -S $SCREENNAME -p 0 -X stuff "$INITAUTHCOMMAND"
		sleep 3
		echo "Now sending quit command..."
		screen -S $SCREENNAME -p 0 -X stuff "$QUITCOMMAND"	
		#sleep 120
		;;
	clean)
		if [ $NUMBER_SAVES -ge 51 ];
		then
			echo "Deleting oldest two saves..."
			echo "deleting $FIRST_SAVE_NAME"
			rm -v $FIRST_SAVE_NAME
			echo "deleting $SECOND_SAVE_NAME"
			rm -v $SECOND_SAVE_NAME
		else
			echo "Nothing to clean, there are only $NUMBER_SAVES savefiles"
		fi
	;;
	startnew)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server is not running."
			echo "Starting $SERVERNAME..."
			screen -d -m -S "$SCREENNAME" $COMMANDFIRSTTIME
		else
			echo "Server is already running."
		fi
		echo -e "Screen session-name: \"$SCREENNAME\""
	;;
	start)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server is not running."
			echo "Starting $SERVERNAME..."
			screen -d -m -S "$SCREENNAME" $COMMAND
		else
			echo "Server is already running."
		fi
		echo -e "Screen session-name: \"$SCREENNAME\""
	;;
	stop)
	if [ "$RUNNING" -eq "1" ];
	then
		echo "Server was not running."
	else
		echo "Server is runnning. Sending save command."
		#echo -e "\"$SAVECOMMAND\""
		screen -S $SCREENNAME -p 0 -X stuff "$SAVECOMMAND"
		echo "Now sending quit command..."
		screen -S $SCREENNAME -p 0 -X stuff "$QUITCOMMAND"	
	fi
	;;
	halt)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server was not running."
		else
			echo "Server is runnning. Sending quit command."
			screen -S $SCREENNAME -p 0 -X stuff "$QUITCOMMAND"	
		fi
	;;
	maintain)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server was not running. Restarting..."
			echo "Starting $SERVERNAME..."
                        screen -d -m -S "$SCREENNAME" $COMMAND	
		else
			echo "Server is runnning. Sending save command."
			#echo -e "\"$SAVECOMMAND\""
			screen -S $SCREENNAME -p 0 -X stuff "$SAVECOMMAND"
		fi
		#sleep 120
		;;

	control)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server was not running."
		else
			echo "Server is runnning. Attaching..."
			screen -r $SCREENNAME
	 	fi
		;;

	save)
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server was not running. "
		else
			echo "Server is runnning. Sending save command."
			#echo -e "\"$SAVECOMMAND\""
			screen -S $SCREENNAME -p 0 -X stuff "$SAVECOMMAND"
		fi
		;;
	command)
		echo -e "Sending command \"$CMD\" to the server"
		if [ "$RUNNING" -eq "1" ];
		then
			echo "Server was not running. "
		else
			echo "Server is runnning. Sending command..."
			rm $SCREENOUTPUTFILE
			screen -S $SCREENNAME -p 0 -X logfile $SCREENOUTPUTFILE
			screen -S $SCREENNAME -p 0 -X log on
			sleep 5
			screen -S $SCREENNAME -p 0 -X stuff "$CMD^M"
			sleep 5
			screen -S $SCREENNAME -p 0 -X log off
			sed -i '1d;$d' $SCREENOUTPUTFILE
		fi

		;;
esac
